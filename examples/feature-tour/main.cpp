#define FASTLED_NEEDS_YIELD
#include "Arduino.h"
#include "myLixie.h"

// Lixie settings
#define DATA_PIN 4
#define NUM_LIXIES 4

/* 
*   Do not edit below
*/

uint32_t display = 11234;
myLixie lix(DATA_PIN, NUM_LIXIES);

void setup()
{
    lix.setup();
#ifdef ESP8266
    ESP.wdtDisable();
    ESP.wdtEnable(WDTO_8S);
#endif
    Serial.begin(115200);
#ifdef ESP8266
    ESP.wdtFeed();
#endif
    delay(2000);
    Serial.println("Starting Demo...");
}

enum fsmState
{
    FSM_SIMPLE_SWITCH = 1,
    FSM_FADE_WO_OVERLAPP,
    FSM_FADE_W_OVERLAPP,
    FSM_SLOT_MACHINE,
    FSM_BLINK,
    FSM_BLINK_FADE,
    FSM_FLASH_FADE,
    FSM_COLOR_BACKGROUND,
    FSM_COLOR_FOREGROUND,
    FSM_RAINBOW_BACKGROUND,
    FSM_RAINBOW_FOREGROUND,
    FSM_BLEND_PALETTE,
    FSM_EVERYTHING,
};

fsmState state = FSM_SIMPLE_SWITCH;
unsigned long stateStartTime = 0;
bool enterState = true;
uint32_t duration = 0;
byte hue = 0;
int red = 0;
bool forward = true;
CRGB paletteA = CRGB::Orange;
CRGB paletteB = CRGB::Aqua;
CEveryNSeconds display1sec(1);
CEveryNSeconds display3sec(3);

void loop()
{
    switch (state)
    {
    case FSM_SIMPLE_SWITCH:
        if (enterState)
        {
            duration = 10000;
            enterState = false;
            stateStartTime = millis();
            lix.clearAll();
            display1sec.reset();
            lix.setGlobalAnimationType(SWITCH);
            lix.setGlobalBackgroundColor(CRGB::Black);
            lix.setGlobalColor(CRGB::White);
            Serial.printf("%d. Simple number changes\n", state);
        }
        if (millis() - stateStartTime <= duration)
        {
            if (display1sec)
            {
                lix.put(display);
                display += 1111;
            }
        }
        else
        {
            enterState = true;
            state = FSM_FADE_WO_OVERLAPP;
            lix.clearAll();
        }
        break;
    case FSM_FADE_WO_OVERLAPP:
        if (enterState)
        {
            duration = 10000;
            enterState = false;
            stateStartTime = millis();
            lix.clearAll();
            lix.setGlobalAnimation(FADE_WO_OVERLAPP, 900);
            lix.setGlobalColor(CRGB::White);
            Serial.printf("%d. Simple number changes with fading without crossover\n", state);
        }
        if (millis() - stateStartTime <= duration)
        {
            if (display1sec)
            {
                lix.put(display);
                display += 1111;
            }
        }
        else
        {
            enterState = true;
            state = FSM_FADE_W_OVERLAPP;
            lix.clearAll();
        }
        break;
    case FSM_FADE_W_OVERLAPP:
        if (enterState)
        {
            duration = 10000;
            enterState = false;
            stateStartTime = millis();
            lix.clearAll();
            lix.setGlobalAnimation(FADE_W_OVERLAPP, 900);
            lix.setGlobalColor(CRGB::White);
            Serial.printf("%d. Simple number changes with fading with crossover\n", state);
        }
        if (millis() - stateStartTime <= duration)
        {
            if (display1sec)
            {
                lix.put(display);
                display += 1111;
            }
        }
        else
        {
            enterState = true;
            state = FSM_SLOT_MACHINE;
            lix.clearAll();
        }
        break;
    case FSM_SLOT_MACHINE:
        if (enterState)
        {
            duration = 10000;
            enterState = false;
            stateStartTime = millis();
            lix.clearAll();
            lix.setGlobalAnimation(SLOT_MACHINE, 300);
            lix.setGlobalColor(CRGB::White);
            Serial.printf("%d. Slot machine-like switches\n", state);
        }
        if (millis() - stateStartTime <= duration)
        {
            if (display1sec)
            {
                lix.put(display);
                display += 1111;
            }
        }
        else
        {
            enterState = true;
            state = FSM_BLINK;
            lix.clearAll();
        }
        break;
    case FSM_BLINK:
        if (enterState)
        {
            duration = 10000;
            enterState = false;
            stateStartTime = millis();
            lix.clearAll();
            lix.setGlobalAnimationType(BLINK);
            lix.setGlobalColor(CRGB::White);
            for (uint8_t i = 0; i < NUM_LIXIES; i++)
            {
                lix.setAnimationDuration(300 + 200 * i, i);
            }

            Serial.printf("%d. Blink, with different blink time for each number\n", state);
        }
        if (millis() - stateStartTime <= duration)
        {
            if (display1sec)
            {
                lix.put(display);
            }
        }
        else
        {
            enterState = true;
            state = FSM_BLINK_FADE;
            lix.clearAll();
        }
        break;
    case FSM_BLINK_FADE:
        if (enterState)
        {
            duration = 10000;
            enterState = false;
            stateStartTime = millis();
            lix.clearAll();
            lix.setGlobalAnimationType(BLINK_FADE);
            lix.setGlobalColor(CRGB::White);
            for (uint8_t i = 0; i < NUM_LIXIES; i++)
            {
                lix.setAnimationDuration(300 + 200 * i, i);
            }

            Serial.printf("%d. Blink with fading, with different blink time for each number\n", state);
        }
        if (millis() - stateStartTime <= duration)
        {
            if (display1sec)
            {
                lix.put(display);
            }
        }
        else
        {
            enterState = true;
            state = FSM_FLASH_FADE;
            lix.clearAll();
        }
        break;
    case FSM_FLASH_FADE:
        if (enterState)
        {
            duration = 10000;
            enterState = false;
            stateStartTime = millis();
            lix.clearAll();
            lix.setGlobalAnimation(FLASH_FADE, 200);
            lix.setGlobalColor(CRGB::Red);

            Serial.printf("%d. Flash number fading back to current color (only works if current color is not White)\n", state);
        }
        if (millis() - stateStartTime <= duration)
        {
            if (display1sec)
            {
                lix.put(display);
                display += 1111;
            }
        }
        else
        {
            enterState = true;
            state = FSM_COLOR_BACKGROUND;
            lix.clearAll();
        }
        break;
    case FSM_COLOR_BACKGROUND:
        if (enterState)
        {
            duration = 20000;
            enterState = false;
            stateStartTime = millis();
            lix.clearAll();
            display3sec.reset();
            lix.setGlobalAnimation(FADE_W_OVERLAPP, 2500);
            lix.setGlobalColor(CRGB::White);
            lix.setGlobalBackgroundColor((CRGB)CHSV(hue, 255, 100));

            Serial.printf("%d. Change the background color while fading between numbers\n", state);
        }
        if (millis() - stateStartTime <= duration)
        {
            if (display3sec)
            {
                lix.put(display);
                display += 1111;
            }
            EVERY_N_MILLIS(20)
            {
                lix.setGlobalBackgroundColor((CRGB)CHSV(hue, 255, 100));
                hue++;
            }
        }
        else
        {
            enterState = true;
            state = FSM_COLOR_FOREGROUND;
            lix.clearAll();
        }
        break;
    case FSM_COLOR_FOREGROUND:
        if (enterState)
        {
            duration = 20000;
            enterState = false;
            stateStartTime = millis();
            lix.clearAll();
            lix.setGlobalAnimation(FADE_W_OVERLAPP, 2500);
            lix.setGlobalBackgroundColor(CRGB::Black);

            Serial.printf("%d. Change the foreground color while fading between numbers\n", state);
        }
        if (millis() - stateStartTime <= duration)
        {
            if (display3sec)
            {
                lix.put(display);
                display += 1111;
            }
            EVERY_N_MILLIS(20)
            {
                lix.setGlobalColor((CRGB)CHSV(hue, 255, 255));
                hue++;
            }
        }
        else
        {
            enterState = true;
            state = FSM_RAINBOW_BACKGROUND;
            lix.clearAll();
        }
        break;
    case FSM_RAINBOW_BACKGROUND:
        if (enterState)
        {
            duration = 20000;
            enterState = false;
            stateStartTime = millis();
            lix.clearAll();
            lix.setGlobalAnimation(FADE_W_OVERLAPP, 2500);
            lix.setGlobalColor(CRGB(255, 255, 255));
            lix.setGlobalBackgroundColor((CRGB)CHSV(hue, 255, 100));

            Serial.printf("%d. Change the background color using rainbow while fading between numbers\n", state);
        }
        if (millis() - stateStartTime <= duration)
        {
            if (display3sec)
            {
                lix.put(display);
                display += 1111;
            }
            EVERY_N_MILLIS(20)
            {
                lix.setGlobalBackgroundColor(CHSV(hue, 255, 100), 20);
                hue++;
            }
        }
        else
        {
            enterState = true;
            state = FSM_RAINBOW_FOREGROUND;
            lix.clearAll();
        }
        break;
    case FSM_RAINBOW_FOREGROUND:
        if (enterState)
        {
            duration = 20000;
            enterState = false;
            stateStartTime = millis();
            lix.clearAll();
            lix.setGlobalAnimation(FADE_W_OVERLAPP, 2500);
            lix.setGlobalBackgroundColor(CRGB::Black);

            Serial.printf("%d. Change the foreground color using rainbow while fading between numbers\n", state);
        }
        if (millis() - stateStartTime <= duration)
        {
            if (display3sec)
            {
                lix.put(display);
                display += 1111;
            }
            EVERY_N_MILLIS(20)
            {
                lix.setGlobalColor(CHSV(hue, 255, 255));
                hue++;
            }
        }
        else
        {
            enterState = true;
            state = FSM_BLEND_PALETTE;
            lix.clearAll();
        }
        break;
    case FSM_BLEND_PALETTE:
        if (enterState)
        {
            duration = 20000;
            enterState = false;
            stateStartTime = millis();
            lix.clearAll();
            lix.setGlobalAnimation(FADE_W_OVERLAPP, 2500);
            lix.setGlobalBackgroundColor(CRGB::Black);
            lix.setGlobalColor(paletteA);

            Serial.printf("%d. Blend between 2 colors\n", state);
        }
        if (millis() - stateStartTime <= duration)
        {
            if (display3sec)
            {
                lix.put(display);
                display += 1111;
            }
            uint8_t ratio = beatsin8(10); // 10 times per minutes;
            lix.setGlobalColor(blend(paletteA, paletteB, ratio));
        }
        else
        {
            enterState = true;
            state = FSM_EVERYTHING;
            lix.clearAll();
        }
        break;
    case FSM_EVERYTHING:
        if (enterState)
        {
            duration = 100000;
            enterState = false;
            stateStartTime = millis();
            lix.clearAll();
            lix.setAnimation(BLINK_FADE, 1000, 0);
            lix.setAnimation(FADE_W_OVERLAPP, 1000, 1);
            lix.setAnimation(FADE_WO_OVERLAPP, 2000, 2);
            lix.setAnimation(SLOT_MACHINE, 300, 3);
            lix.setGlobalBackgroundColor(CRGB::Black);
            Serial.printf("%d. Do everything together\n", state);
        }
        if (millis() - stateStartTime <= duration)
        {
            if (display3sec)
            {
                lix.put(display);
                display += 1111;
            }
            EVERY_N_MILLIS(20)
            {
                lix.setGlobalColor(CHSV(hue, 255, 255));
                hue++;
            }
            EVERY_N_MILLIS(20)
            {
                uint8_t val = sin8(++red);
                CRGB color = CRGB(val, val, val).nscale8(30);
                lix.setGlobalBackgroundColor(color);
            }
        }
        else
        {
            enterState = true;
            state = FSM_SIMPLE_SWITCH;
            lix.clearAll();
        }
        break;
    }

    // Update the display every 3ms to avoid refresh issues on leds;
    lix.update();
    yield();
#ifdef ESP8266
    ESP.wdtFeed();
#endif
}