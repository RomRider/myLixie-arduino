#include "myLixie.h"

void myLixie::put(uint32_t number)
{
    //Serial.printf("Start\n");
    uint8_t index = 0;
    uint8_t digit;
    unsigned long timeNow = millis();
    if (number == 0)
    {
        if (this->newInput[index] != 0)
        {
            this->newInput[index] = '0';
            this->updateStartTimeHelper(index, timeNow);
        }
        this->newInput[0] = '0';
        ++index;
    }
    while (number > 0 && index < this->numDigits)
    {
        digit = number % 10;
        number /= 10;
        if (this->newInput[index] != digit + 48)
        {
            this->newInput[index] = digit + 48;
            this->updateStartTimeHelper(index, timeNow);
        }
        ++index;
    }
    if (index < this->numDigits)
    {
        for (uint8_t i = index; i < this->numDigits; ++i)
        {
            if (this->newInput[index] != 0)
            {
                this->newInput[index] = 0;
                this->updateStartTimeHelper(index, timeNow);
            }
        }
    }
    //this->fadeLastUpdate = millis();
    //Serial.printf("fadeStartTime = %l\n", this->fadeStartTime);
}

void myLixie::put(const char *stringDigit)
{
    uint8_t lixie = 0;
    for (; lixie < this->numDigits && lixie < sizeof(stringDigit); ++lixie)
    {
        this->put(stringDigit[lixie], lixie);
    }
    if (lixie == this->numDigits)
    {
        return;
    }
    for (; lixie < this->numDigits; ++lixie)
    {
        this->put((char)0, lixie);
    }
}

void myLixie::put(const char charDigit, const uint8_t lixie)
{
    unsigned long timeNow = millis();
    if (charDigit - 48 <= 9) // char to int
    {
        if (this->newInput[lixie] != charDigit)
        {
            this->newInput[lixie] = charDigit;
            this->updateStartTimeHelper(lixie, timeNow);
        }
    }
    else
    {
        if (this->newInput[lixie] != 0)
        {
            this->newInput[lixie] = 0;
            this->updateStartTimeHelper(lixie, timeNow);
        }
    }
}