#include "myLixie.h"

void myLixie::setGlobalAnimationDuration(const uint16_t duration)
{
    for (uint8_t lixie = 0; lixie < this->numDigits; lixie++)
    {
        this->setAnimationDuration(duration, lixie);
    }
}

void myLixie::setAnimationDuration(const uint16_t duration, const uint8_t lixie)
{
    this->animationDuration[lixie] = duration;
}

void myLixie::setGlobalAnimationType(const animationType type)
{
    for (uint8_t lixie = 0; lixie < this->numDigits; lixie++)
    {
        this->setAnimationType(type, lixie);
    }
}

void myLixie::setAnimationType(const animationType type, const uint8_t lixie)
{
    this->lixieAnim[lixie] = type;
}

void myLixie::setAnimation(const animationType type, const uint16_t duration, const uint8_t lixie)
{
    this->lixieAnim[lixie] = type;
    this->animationDuration[lixie] = duration;
}

void myLixie::setGlobalAnimation(const animationType type, const uint16_t duration)
{
    for (uint8_t lixie = 0; lixie < this->numDigits; lixie++)
    {
        this->lixieAnim[lixie] = type;
        this->animationDuration[lixie] = duration;
    }
}

void myLixie::updateStartTimeHelper(const uint8_t lixie, const unsigned long timeNow)
{
    switch (this->lixieAnim[lixie])
    {
    case BLINK:
        break;
    case BLINK_FADE:
        break;
    default:
        this->animStartTime[lixie] = timeNow;
        break;
    }
}

void myLixie::update()
{
    //this->fadeTimeSinceStart += millis() - this->fadeLastUpdate;
    unsigned long timeNow = millis();
    for (uint8_t lixie = 0; lixie < this->numDigits; ++lixie)
    {
        uint8_t newDigit = this->newInput[lixie] - 48;
        uint8_t currentDigit = this->currentInput[lixie] - 48;
        switch (this->lixieAnim[lixie])
        {
        case SWEEP:
            this->sweepAnimation(lixie, timeNow);
            break;
        case FADE_W_OVERLAPP:
            this->fadeWithTransistion(currentDigit, newDigit, lixie, timeNow);
            break;
        case FADE_WO_OVERLAPP:
            this->fadeWithoutTransistion(currentDigit, newDigit, lixie, timeNow);
            break;
        case SWITCH:
            this->switchNumber(currentDigit, newDigit, lixie, timeNow);
            break;
        case SLOT_MACHINE:
            this->slotMachineAnimation(currentDigit, newDigit, lixie, timeNow);
            break;
        case BLINK:
            this->blinkAnimation(currentDigit, newDigit, lixie, timeNow);
            break;
        case BLINK_FADE:
            this->blinkFadeAnimation(currentDigit, newDigit, lixie, timeNow);
            break;
        case FLASH_FADE:
            this->flashFadeAnimation(currentDigit, newDigit, lixie, timeNow);
            break;
        default:
            this->switchNumber(currentDigit, newDigit, lixie, timeNow);
            break;
        }
    }
    EVERY_N_MILLIS(4)
    {
        this->controller->showLeds(this->globalBrightness);
    }
}

void myLixie::fadeWithTransistion(const uint8_t currentDigit, const uint8_t newDigit, const uint8_t lixie, const unsigned long timeNow)
{
    if (newDigit == currentDigit)
    {
        this->endAnimation(currentDigit, lixie);
        return;
    }
    CRGB foregroundColor = this->globalDigitColors[lixie];
    CRGB backGroundColor = this->globalDigitBackgroundColors[lixie];
    CRGB newDigitColor;
    CRGB currentDigitColor;
    unsigned long fadeTimeFromStart = timeNow - this->animStartTime[lixie];
    if (fadeTimeFromStart <= this->animationDuration[lixie])
    {
        for (uint8_t i = 0; i < 10; ++i)
        {
            this->clearDigit(i, lixie);
        }
        if (currentDigit < 10)
        {
            currentDigitColor.r = map(fadeTimeFromStart, 0, this->animationDuration[lixie], foregroundColor.r, backGroundColor.r);
            currentDigitColor.g = map(fadeTimeFromStart, 0, this->animationDuration[lixie], foregroundColor.g, backGroundColor.g);
            currentDigitColor.b = map(fadeTimeFromStart, 0, this->animationDuration[lixie], foregroundColor.b, backGroundColor.b);
            // Serial.printf("background r=%i, g=%i, b=%i\n", backGroundColor.r, backGroundColor.g, backGroundColor.b);
            // Serial.printf("foreground r=%i, g=%i, b=%i\n", foregroundColor.r, foregroundColor.g, foregroundColor.b);
            // Serial.printf("colors old r=%i, g=%i, b=%i\n", currentDigitColor.r, currentDigitColor.g, currentDigitColor.b);
            this->putDigitWithColor(currentDigit, lixie, currentDigitColor);
        }
        if (newDigit < 10)
        {
            newDigitColor.r = map(fadeTimeFromStart, 0, this->animationDuration[lixie], backGroundColor.r, foregroundColor.r);
            newDigitColor.g = map(fadeTimeFromStart, 0, this->animationDuration[lixie], backGroundColor.g, foregroundColor.g);
            newDigitColor.b = map(fadeTimeFromStart, 0, this->animationDuration[lixie], backGroundColor.b, foregroundColor.b);
            // Serial.printf("colors new r=%i, g=%i, b=%i\n", newDigitColor.r, newDigitColor.g, newDigitColor.b);
            this->putDigitWithColor(newDigit, lixie, newDigitColor);
        }
    }
    else
    {
        this->endAnimation(newDigit, lixie);
    }
}

void myLixie::fadeWithoutTransistion(const uint8_t currentDigit, const uint8_t newDigit, const uint8_t lixie, const unsigned long timeNow)
{
    if (newDigit == currentDigit)
    {
        this->endAnimation(currentDigit, lixie);
        return;
    }
    CRGB foregroundColor = this->globalDigitColors[lixie];
    CRGB backGroundColor = this->globalDigitBackgroundColors[lixie];
    CRGB newDigitColor;
    CRGB currentDigitColor;
    unsigned long fadeTimeFromStart = timeNow - this->animStartTime[lixie];
    if (fadeTimeFromStart <= this->animationDuration[lixie] / 2)
    {
        for (uint8_t i = 0; i < 10; ++i)
        {
            this->clearDigit(i, lixie);
        }
        if (currentDigit < 10)
        {
            currentDigitColor.r = map(fadeTimeFromStart, 0, this->animationDuration[lixie] / 2, foregroundColor.r, backGroundColor.r);
            currentDigitColor.g = map(fadeTimeFromStart, 0, this->animationDuration[lixie] / 2, foregroundColor.g, backGroundColor.g);
            currentDigitColor.b = map(fadeTimeFromStart, 0, this->animationDuration[lixie] / 2, foregroundColor.b, backGroundColor.b);
            // Serial.printf("background r=%i, g=%i, b=%i\n", backGroundColor.r, backGroundColor.g, backGroundColor.b);
            // Serial.printf("foreground r=%i, g=%i, b=%i\n", foregroundColor.r, foregroundColor.g, foregroundColor.b);
            // Serial.printf("colors old r=%i, g=%i, b=%i\n", currentDigitColor.r, currentDigitColor.g, currentDigitColor.b);
            this->putDigitWithColor(currentDigit, lixie, currentDigitColor);
        }
    }
    else if (fadeTimeFromStart > this->animationDuration[lixie] / 2 && fadeTimeFromStart <= this->animationDuration[lixie])
    {
        for (uint8_t i = 0; i < 10; ++i)
        {
            this->clearDigit(i, lixie);
        }
        if (newDigit < 10)
        {
            newDigitColor.r = map(fadeTimeFromStart, this->animationDuration[lixie] / 2, this->animationDuration[lixie], backGroundColor.r, foregroundColor.r);
            newDigitColor.g = map(fadeTimeFromStart, this->animationDuration[lixie] / 2, this->animationDuration[lixie], backGroundColor.g, foregroundColor.g);
            newDigitColor.b = map(fadeTimeFromStart, this->animationDuration[lixie] / 2, this->animationDuration[lixie], backGroundColor.b, foregroundColor.b);
            // Serial.printf("colors new r=%i, g=%i, b=%i\n", newDigitColor.r, newDigitColor.g, newDigitColor.b);
            this->putDigitWithColor(newDigit, lixie, newDigitColor);
        }
    }
    else
    {
        this->endAnimation(newDigit, lixie);
    }
}

void myLixie::switchNumber(const uint8_t currentDigit, const uint8_t newDigit, const uint8_t lixie, const unsigned long timeNow)
{
    this->endAnimation(newDigit, lixie);
}

void myLixie::slotMachineAnimation(const uint8_t currentDigit, const uint8_t newDigit, const uint8_t lixie, const unsigned long timeNow)
{
    if (newDigit == currentDigit)
    {
        this->endAnimation(currentDigit, lixie);
        return;
    }
    unsigned long timeFromStart = timeNow - this->animStartTime[lixie];
    //Serial.printf("animDuration = %i, timeFromStart = %i\n", this->animationDuration[lixie], timeFromStart);

    if (timeFromStart <= this->animationDuration[lixie])
    {
        uint8_t iter = map(timeFromStart, 0, this->animationDuration[lixie], 1, 10);
        uint8_t toPrint = currentDigit <= 9 ? (currentDigit + iter) % 10 : (newDigit + iter) % 10;
        //Serial.printf("currentDigit = %i, nextDigit = %i, toPrint = %i\n", currentDigit, newDigit, toPrint);
        for (uint8_t digit = 0; digit < 10; ++digit)
        {
            this->clearDigit(digit, lixie);
        }
        this->putDigit(toPrint, lixie);
    }
    else
    {
        this->endAnimation(newDigit, lixie);
    }
}

void myLixie::endAnimation(const uint8_t newDigit, const uint8_t lixie)
{
    if (newDigit < 10)
    {
        this->putOneDigit(newDigit, lixie);
    }
    else
    {
        this->clearOneDigit(lixie);
    }
    this->currentInput[lixie] = this->newInput[lixie];
}

void myLixie::sweepAnimationInit(const uint16_t duration)
{
    this->sweepForward = true;
    this->sweepLixie = 0;
    this->setGlobalAnimationType(SWEEP);
    this->setGlobalAnimationDuration(duration / this->numDigits);
    unsigned long timeNow = millis();
    for (uint8_t lixie = 0; lixie < this->numDigits; ++lixie)
    {
        this->currentInput[lixie] = '\0';
        this->newInput[lixie] = '\0';
        this->animStartTime[lixie] = timeNow;
    }
}

void myLixie::sweepAnimation(const uint8_t lixie, const unsigned long timeNow)
{
    //Serial.printf("sL = %i - sW = %i, sP = %i\n", this->sweepLixie, this->sweepForward, this->sweepPosition);
    unsigned long timeFromStart = timeNow - this->animStartTime[lixie];
    if (timeFromStart <= this->animationDuration[lixie])
    {
        if (lixie == this->sweepLixie)
        {
            uint8_t iter;
            if (this->sweepForward)
            {
                iter = map(timeFromStart, 0, this->animationDuration[lixie], 1, 8);
            }
            else
            {
                iter = map(timeFromStart, 0, this->animationDuration[lixie], 7, 0);
            }
            //Serial.printf("lixie[%i] -> iter = %i\n", lixie, iter);
            for (uint8_t pos = 0; pos < 10; ++pos)
            {
                if (pos >= iter && pos < iter + 3)
                {
                    this->putDigit(this->sweepOrder[pos], lixie);
                }
                else
                {
                    this->clearDigit(this->sweepOrder[pos], lixie);
                }
            }
        }
        else
        {
            this->clearOneDigit(lixie);
        }
    }
    else
    {
        this->clearOneDigit(lixie);
        this->sweepLixie = this->sweepForward ? this->sweepLixie + 1 : this->sweepLixie - 1;
        if (this->sweepLixie > this->numDigits - 1)
        {
            this->sweepLixie = this->numDigits - 1;
            this->sweepForward = false;
        }
        if (this->sweepLixie < 0)
        {
            this->sweepLixie = 0;
            this->sweepForward = true;
        }
        for (uint8_t lixie = 0; lixie < this->numDigits; ++lixie)
        {
            this->animStartTime[lixie] = timeNow;
        }
    }
}

void myLixie::blinkAnimation(const uint8_t currentDigit, const uint8_t newDigit, const uint8_t lixie, const unsigned long timeNow)
{
    this->currentInput[lixie] = newDigit;
    unsigned long timeFromStart = timeNow - this->animStartTime[lixie];
    if (timeFromStart <= this->animationDuration[lixie] / 2)
    {
        this->putOneDigit(newDigit, lixie);
    }
    else if (timeFromStart > this->animationDuration[lixie] / 2 && timeFromStart <= this->animationDuration[lixie])
    {
        this->clearOneDigit(lixie);
    }
    else
    {
        this->animStartTime[lixie] = timeNow;
    }
}

void myLixie::blinkFadeAnimation(const uint8_t currentDigit, const uint8_t newDigit, const uint8_t lixie, const unsigned long timeNow)
{
    this->currentInput[lixie] = newDigit;
    unsigned long timeFromStart = timeNow - this->animStartTime[lixie];
    CRGB foregroundColor = this->globalDigitColors[lixie];
    CRGB backGroundColor = this->globalDigitBackgroundColors[lixie];
    CRGB newDigitColor;
    if (timeFromStart <= this->animationDuration[lixie] / 2)
    {
        newDigitColor.r = map(timeFromStart, 0, this->animationDuration[lixie] / 2, foregroundColor.r, backGroundColor.r);
        newDigitColor.g = map(timeFromStart, 0, this->animationDuration[lixie] / 2, foregroundColor.g, backGroundColor.g);
        newDigitColor.b = map(timeFromStart, 0, this->animationDuration[lixie] / 2, foregroundColor.b, backGroundColor.b);
        this->putOneDigitWithColor(newDigit, lixie, newDigitColor);
    }
    else if (timeFromStart > this->animationDuration[lixie] / 2 && timeFromStart <= this->animationDuration[lixie])
    {
        newDigitColor.r = map(timeFromStart, this->animationDuration[lixie] / 2, this->animationDuration[lixie], backGroundColor.r, foregroundColor.r);
        newDigitColor.g = map(timeFromStart, this->animationDuration[lixie] / 2, this->animationDuration[lixie], backGroundColor.g, foregroundColor.g);
        newDigitColor.b = map(timeFromStart, this->animationDuration[lixie] / 2, this->animationDuration[lixie], backGroundColor.b, foregroundColor.b);
        this->putOneDigitWithColor(newDigit, lixie, newDigitColor);
    }
    else
    {
        this->animStartTime[lixie] = timeNow;
    }
}

void myLixie::flashFadeAnimation(const uint8_t currentDigit, const uint8_t newDigit, const uint8_t lixie, const unsigned long timeNow)
{
    if (newDigit == currentDigit)
    {
        this->endAnimation(currentDigit, lixie);
        return;
    }
    unsigned long timeFromStart = timeNow - this->animStartTime[lixie];
    CRGB foregroundColor = this->globalDigitColors[lixie];
    CRGB backGroundColor = this->globalDigitBackgroundColors[lixie];
    CRGB newDigitColor;
    if (timeFromStart <= this->animationDuration[lixie])
    {
        newDigitColor.r = map(timeFromStart, 0, this->animationDuration[lixie], 255, foregroundColor.r);
        newDigitColor.g = map(timeFromStart, 0, this->animationDuration[lixie], 255, foregroundColor.g);
        newDigitColor.b = map(timeFromStart, 0, this->animationDuration[lixie], 255, foregroundColor.b);
        this->putOneDigitWithColor(newDigit, lixie, newDigitColor);
    }
    else
    {
        this->endAnimation(newDigit, lixie);
    }
}

void myLixie::shiftDisplay(const bool toTheLeft)
{
    char *tempDisplay = new char(this->numDigits);

    for (uint8_t i = 0; i < this->numDigits; ++i)
    {
        tempDisplay[i] = this->newInput[i];
    }

    for (uint8_t lixie = 0; lixie < this->numDigits; ++lixie)
    {
        if (toTheLeft)
        {
            this->put(tempDisplay[lixie], mod8((lixie + 1), this->numDigits));
        }
        else
        {
            this->put(tempDisplay[lixie], mod8((uint8_t)(lixie - 1), this->numDigits));
        }
    }
}