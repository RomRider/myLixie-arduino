#include "myLixie.h"

constexpr byte myLixie::addresses[];
constexpr byte myLixie::addressesReverse[];
constexpr byte myLixie::sweepOrder[];

myLixie::myLixie(const uint8_t pin, uint8_t nDigits) : numDigits(nDigits), numLEDs(nDigits * LEDsPerDigit)
{
    this->leds = new CRGB[numLEDs];
    this->globalDigitColors = new CRGB[numDigits];
    this->globalDigitBackgroundColors = new CRGB[numDigits];
    this->globalBrightness = 255;
    this->currentInput = new char[numDigits];
    this->newInput = new char[numDigits];
    this->currentInput = new char[numDigits];
    this->lixieAnim = new animationType[numDigits];
    this->animationDuration = new uint16_t[numDigits];
    this->animStartTime = new unsigned long[numDigits];
    this->buildController(pin);
    this->setColorCorrection(TypicalPixelString);
}

void myLixie::setup()
{
    for (uint8_t i = 0; i < this->numDigits; ++i)
    {
        this->globalDigitColors[i] = CRGB(255, 255, 255);
        this->globalDigitBackgroundColors[i] = CRGB(0, 0, 0);
        this->newInput[i] = 0;
        this->currentInput[i] = 0;
        this->lixieAnim[i] = SWITCH;
        this->animationDuration[i] = 500;
        this->animStartTime[i] = 0;
    }
    this->setWhiteBalance(UncorrectedTemperature);
}

void myLixie::setWhiteBalance(const CRGB c_adj) { this->controller->setTemperature(c_adj); }

void myLixie::setColorCorrection(const CRGB c_corr) { this->controller->setCorrection(c_corr); }

void myLixie::putOneDigit(const uint8_t digit, const uint8_t lixieIndex)
{
    //Serial.printf("r %i, g %i, b %i\n", this->globalDigitColors[lixieIndex].r, this->globalDigitColors[lixieIndex].g, this->globalDigitColors[lixieIndex].b);
    this->putOneDigitWithColor(digit, lixieIndex, this->globalDigitColors[lixieIndex]);
}

void myLixie::putOneDigitWithColor(const uint8_t digit, const uint8_t lixieIndex, const CRGB color)
{
    //this->currentInput[lixieIndex] = digit + 48;
    for (uint8_t i = 0; i < 10; ++i)
    {
        if (i == digit)
        {
            //Serial.printf("putOneDigitWithColor: digit %i, index %i, color %i %i %i\n", digit, lixieIndex, color.r, color.g, color.b);
            this->putDigitWithColor(digit, lixieIndex, color);
        }
        else
        {
            this->clearDigit(i, lixieIndex);
        }
    }
}

void myLixie::clearOneDigit(const uint8_t lixieIndex)
{
    //this->currentInput[lixieIndex] = '\0';
    for (uint8_t i = 0; i < 10; ++i)
    {
        this->clearDigit(i, lixieIndex);
    }
}

/*
* Buffer a digit with the current lixie color
*/
void myLixie::putDigit(const uint8_t digit, const uint8_t lixieIndex)
{
    this->putDigitWithColor(digit, lixieIndex, this->globalDigitColors[lixieIndex]);
}

/*
* Buffer a digit with it's color set
*/
void myLixie::putDigitWithColor(const uint8_t digit, const uint8_t lixieIndex, const CRGB color)
{
    uint16_t ledIndex = lixieIndex * this->LEDsPerDigit + this->addresses[mod8(digit, 10)];
    this->leds[ledIndex] = color;
    this->leds[ledIndex + 10] = color;
}

void myLixie::clearDigit(const uint8_t digit, const uint8_t lixieIndex)
{
    uint16_t ledIndex = lixieIndex * this->LEDsPerDigit + this->addresses[mod8(digit, 10)];
    this->leds[ledIndex] = this->globalDigitBackgroundColors[lixieIndex];
    this->leds[ledIndex + 10] = this->globalDigitBackgroundColors[lixieIndex];
}

/*
* Set the globalBrightness to brightness
*/
void myLixie::setGlobalBrightness(const uint8_t brightness)
{
    this->globalBrightness = brightness;
    //this->controller->show(brightness);
}

void myLixie::clearAll()
{
    memset8(this->currentInput, 0, this->numDigits * sizeof(char));
    memset8(this->newInput, 0, this->numDigits * sizeof(char));
}

/*
* Initialize the FastLED Controller
*/
void myLixie::buildController(const uint8_t pin)
{
#ifdef __AVR__
    if (pin == 0)
        controller = &FastLED.addLeds<LED_TYPE, 0, COLOR_ORDER>(this->leds, this->numLEDs);
    else if (pin == 1)
        controller = &FastLED.addLeds<LED_TYPE, 1, COLOR_ORDER>(this->leds, this->numLEDs);
    else if (pin == 2)
        controller = &FastLED.addLeds<LED_TYPE, 2, COLOR_ORDER>(this->leds, this->numLEDs);
    else if (pin == 3)
        controller = &FastLED.addLeds<LED_TYPE, 3, COLOR_ORDER>(this->leds, this->numLEDs);
    else if (pin == 4)
        controller = &FastLED.addLeds<LED_TYPE, 4, COLOR_ORDER>(this->leds, this->numLEDs);
    else if (pin == 5)
        controller = &FastLED.addLeds<LED_TYPE, 5, COLOR_ORDER>(this->leds, this->numLEDs);
    else if (pin == 6)
        controller = &FastLED.addLeds<LED_TYPE, 6, COLOR_ORDER>(this->leds, this->numLEDs);
    else if (pin == 7)
        controller = &FastLED.addLeds<LED_TYPE, 7, COLOR_ORDER>(this->leds, this->numLEDs);
    else if (pin == 8)
        controller = &FastLED.addLeds<LED_TYPE, 8, COLOR_ORDER>(this->leds, this->numLEDs);
    else if (pin == 9)
        controller = &FastLED.addLeds<LED_TYPE, 9, COLOR_ORDER>(this->leds, this->numLEDs);
    else if (pin == 10)
        controller = &FastLED.addLeds<LED_TYPE, 10, COLOR_ORDER>(this->leds, this->numLEDs);
    else if (pin == 11)
        controller = &FastLED.addLeds<LED_TYPE, 11, COLOR_ORDER>(this->leds, this->numLEDs);
    else if (pin == 12)
        controller = &FastLED.addLeds<LED_TYPE, 12, COLOR_ORDER>(this->leds, this->numLEDs);
    else if (pin == 13)
        controller = &FastLED.addLeds<LED_TYPE, 13, COLOR_ORDER>(this->leds, this->numLEDs);
#elif ESP8266
    if (pin == 0)
        controller = &FastLED.addLeds<LED_TYPE, 0, COLOR_ORDER>(this->leds, this->numLEDs);
    else if (pin == 2)
        controller = &FastLED.addLeds<LED_TYPE, 2, COLOR_ORDER>(this->leds, this->numLEDs);
    else if (pin == 4)
        controller = &FastLED.addLeds<LED_TYPE, 4, COLOR_ORDER>(this->leds, this->numLEDs);
    else if (pin == 5)
        controller = &FastLED.addLeds<LED_TYPE, 5, COLOR_ORDER>(this->leds, this->numLEDs);
#endif
}