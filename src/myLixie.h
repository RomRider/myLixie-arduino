/*
Lixie.h - Library for driving Lixie displays!
Created by Connor Nishijima, October 26th 2016.
Released under the GPLv3 license.
*/

#ifndef mylixie_h
#define mlixie_h

#include "Arduino.h"

#ifdef ESP8266
#define FASTLED_ESP8266_RAW_PIN_ORDER
#define FASTLED_ALLOW_INTERRUPTS 0
#define FASTLED_INTERRUPT_RETRY_COUNT 0
#endif

#include "FastLED.h"
// Check if FastLED version is sufficient
#if FASTLED_VERSION < 3000000
#error \
    "Lixie requires FastLED 3.0.0 or later: https://github.com/FastLED/FastLED"
#endif

#ifndef LED_TYPE
#define LED_TYPE WS2812B
#endif

#ifndef COLOR_ORDER
#define COLOR_ORDER GRB
#endif

enum animationType
{
    SWITCH,
    FADE_WO_OVERLAPP,
    FADE_W_OVERLAPP,
    SLOT_MACHINE,
    SWEEP,
    BLINK,
    BLINK_FADE,
    FLASH_FADE,
};

class myLixie
{
  public:
    typedef enum {
        Nixie,
    } predefinedColor;

    myLixie(const uint8_t pin, uint8_t nDigits);
    void setup();

    void setWhiteBalance(const CRGB c_adj);
    void setColorCorrection(const CRGB c_corr);

    void setGlobalColor(const CRGB color);
    void setGlobalColor(CHSV color, const uint8_t stepInterLixie = 20);
    void setColor(const CRGB color, const uint8_t lixie);

    void setGlobalBackgroundColor(const CRGB color);
    void setGlobalBackgroundColor(CHSV color, const uint8_t stepInterLixie = 20);
    void setBackgroundColor(const CRGB color, const uint8_t lixie);

    void setPredefinedColors(predefinedColor pcolor);

    void setGlobalBrightness(const uint8_t brightness);

    void clearAll();

    void put(uint32_t number);
    void put(const char charDigit, const uint8_t lixie);
    void put(const char *stringDigit);

    void setGlobalAnimationType(const animationType type);
    void setAnimationType(const animationType type, uint8_t lixie);
    void setGlobalAnimationDuration(const uint16_t duration);
    void setAnimationDuration(const uint16_t duration, const uint8_t lixie);
    void setAnimation(const animationType type, const uint16_t duration, const uint8_t lixie);
    void setGlobalAnimation(const animationType type, const uint16_t duration);
    void sweepAnimationInit(const uint16_t duration);
    void shiftDisplay(const bool toTheLeft = true);

    void update();

  private:
    void putOneDigit(const uint8_t digit, const uint8_t lixieIndex);
    void putOneDigitWithColor(const uint8_t digit, const uint8_t lixieIndex, const CRGB color);
    void clearOneDigit(const uint8_t lixieIndex);

    void putDigit(const uint8_t digit, const uint8_t lixieIndex);
    void putDigitWithColor(const uint8_t digit, const uint8_t index, const CRGB color);
    void clearDigit(const uint8_t digit, const uint8_t lixieIndex);

    void updateStartTimeHelper(const uint8_t lixie, const unsigned long timeNow);
    void fadeWithTransistion(const uint8_t currentDigit, const uint8_t newDigit, const uint8_t lixie, const unsigned long timeNow);
    void fadeWithoutTransistion(const uint8_t currentDigit, const uint8_t newDigit, const uint8_t lixie, const unsigned long timeNow);
    void switchNumber(const uint8_t currentDigit, const uint8_t newDigit, const uint8_t lixie, const unsigned long timeNow);
    void slotMachineAnimation(const uint8_t currentDigit, const uint8_t newDigit, const uint8_t lixie, const unsigned long timeNow);
    void blinkAnimation(const uint8_t currentDigit, const uint8_t newDigit, const uint8_t lixie, const unsigned long timeNow);
    void blinkFadeAnimation(const uint8_t currentDigit, const uint8_t newDigit, const uint8_t lixie, const unsigned long timeNow);
    void flashFadeAnimation(const uint8_t currentDigit, const uint8_t newDigit, const uint8_t lixie, const unsigned long timeNow);
    void sweepAnimation(const uint8_t lixie, const unsigned long timeNow);
    void endAnimation(const uint8_t newDigit, const uint8_t lixie);

    void buildController(const uint8_t pin);

    CLEDController *controller;

    static constexpr byte addresses[10] = {3, 4, 2, 0, 8, 6, 5, 7, 9, 1};
    static constexpr byte addressesReverse[10] = {3, 9, 2, 0, 1, 6, 5, 7, 4, 8};
    const static uint8_t LEDsPerDigit = 20;
    const uint8_t numDigits;
    const uint16_t numLEDs;

    CRGB *leds;

    CRGB *globalDigitColors;
    CRGB *globalDigitBackgroundColors;
    enum animationType *lixieAnim;

    bool globalBrightnessEnabled = true;
    uint8_t globalBrightness = 255;

    static constexpr byte sweepOrder[10] = {6, 5, 7, 4, 8, 3, 9, 2, 0, 1};
    int16_t sweepPosition = 1;
    int16_t sweepLixie = 0;
    bool sweepForward = true;

    char *newInput;
    char *currentInput;

    uint16_t *animationDuration;
    unsigned long *animStartTime;
};

#endif
