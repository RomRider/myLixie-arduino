#include "myLixie.h"

void myLixie::setGlobalColor(const CRGB color)
{
    for (uint8_t digit = 0; digit < this->numDigits; ++digit)
    {
        this->globalDigitColors[digit] = color;
    }
}

void myLixie::setGlobalBackgroundColor(const CRGB color)
{
    for (uint8_t digit = 0; digit < this->numDigits; ++digit)
    {
        this->globalDigitBackgroundColors[digit] = color;
    }
}

void myLixie::setGlobalColor(CHSV color, const uint8_t stepInterLixie)
{
    for (uint8_t i = 0; i < this->numDigits; ++i)
    {
        this->globalDigitColors[i] = color;
        color.hue += stepInterLixie;
    }
}

void myLixie::setGlobalBackgroundColor(CHSV color, const uint8_t stepInterLixie)
{
    for (uint8_t i = 0; i < this->numDigits; ++i)
    {
        this->globalDigitBackgroundColors[i] = color;
        color.hue += stepInterLixie;
    }
}

void myLixie::setColor(const CRGB color, const uint8_t lixie)
{
    this->globalDigitColors[lixie] = color;
}

void myLixie::setBackgroundColor(const CRGB color, const uint8_t lixie)
{
    this->globalDigitBackgroundColors[lixie] = color;
}

void myLixie::setPredefinedColors(predefinedColor pcolor)
{
    switch (pcolor)
    {
    case predefinedColor::Nixie:
        this->setGlobalBackgroundColor(CRGB(0, 3, 8));
        this->setGlobalColor(CRGB(255, 70, 15));
        break;
    default:
        break;
    }
}